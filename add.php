<?php
require 'inc/Database.php';
require 'inc/Newsletter.php';

if (!empty($_POST)) {
    $email = $_POST['signup-email'];

    Newsletter::register($email);
}