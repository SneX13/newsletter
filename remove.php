<?php
require 'inc/Database.php';
require 'inc/Unsubscribe.php';

if (!empty($_POST)) {
    $email = $_POST['remove-email'];

    Unsubscribe::remove($email);
}