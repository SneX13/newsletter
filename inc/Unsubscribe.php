<?php
class Unsubscribe
{
    private static $email;

    private static $valid = true;

    public function __construct() {
        die('Init function is not allowed');
    }

    public static function remove($email) {
        if (!empty($_POST)) {
            self::$email = $_POST['remove-email'];
    

            if (empty(self::$email)) {
                $status  = "error";
                $message = "The email address field must not be blank";
                self::$valid = false;
            } else if (!filter_var(self::$email, FILTER_VALIDATE_EMAIL)) {
                $status  = "error";
                $message = "You must fill the field with a valid email address";
                self::$valid = false;
            }

            if (self::$valid) {
                $pdo = Database::connect();
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $existingSignup = $pdo->prepare("SELECT COUNT(*) FROM newsletter_registrations WHERE signup_email='$email'");
                $existingSignup->execute();
                $data_exists = ($existingSignup->fetchColumn() > 0) ? true : false;

                if ($data_exists) {
                    $sql = "DELETE FROM newsletter_registrations WHERE signup_email=:email LIMIT 1";
                    $q = $pdo->prepare($sql);

                    $q->execute(
                        array(':email' => self::$email));

                    if ($q) {
                        $status  = "success";
                        $message = "You have been successfully removed from subscription list";
                    } else {
                        $status  = "error";
                        $message = "An error occurred, please try again";
                    }
                } else {
                    $status  = "error";
                    $message = "You have not subscribed with this email";
                }
            }

            $data = array(
                'status'  => $status,
                'message' => $message
            );

            echo json_encode($data);

            Database::disconnect();
        }
    }
}